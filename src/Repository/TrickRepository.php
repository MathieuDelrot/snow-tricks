<?php

namespace App\Repository;

use App\Entity\Trick;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @method Trick|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trick|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trick[]    findAll()
 * @method Trick[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrickRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Trick::class);
    }


    public function findAllPaginate(int $page) {

        if ($page < 1) {
            throw new \InvalidArgumentException('This page does not exist');
        }

        $queryBuilder = $this->createQueryBuilder('c')
            ->orderBy('c.id', 'DESC');

        $query = $queryBuilder->getQuery();

        $firstResult = ($page - 1) * 9;
        $query->setFirstResult($firstResult)->setMaxResults( 9);
        $paginator = new Paginator($query);

        if ( ($paginator->count() <= $firstResult) && $page != 1) {
            throw new NotFoundHttpException('This page does not exist'); // page 404, sauf pour la première page
        }

        return $paginator;

    }

}
