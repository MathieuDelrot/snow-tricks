<?php

namespace App\Repository;

use App\Entity\TricksType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TricksType|null find($id, $lockMode = null, $lockVersion = null)
 * @method TricksType|null findOneBy(array $criteria, array $orderBy = null)
 * @method TricksType[]    findAll()
 * @method TricksType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TricksTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TricksType::class);
    }

}
