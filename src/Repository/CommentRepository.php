<?php

namespace App\Repository;

use App\Entity\Comment;
use App\Entity\Trick;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    public function findAllPaginateByTrick(int $page, Trick $trick) {

        if ($page < 1) {
            throw new \InvalidArgumentException('This page does not exist');
        }

        $queryBuilder = $this->createQueryBuilder('c')
            ->where('c.trick = :trick')
            ->orderBy('c.id', 'DESC')
            ->setParameter('trick', $trick);

        $query = $queryBuilder->getQuery();

        $firstResult = ($page - 1) * 2;
        $query->setFirstResult($firstResult)->setMaxResults( 2);
        $paginator = new Paginator($query);

        if ( ($paginator->count() <= $firstResult) && $page != 1) {
            throw new NotFoundHttpException('This page does not exist'); // page 404, sauf pour la première page
        }

        return $paginator;

    }

}
