<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Image;
use App\Entity\Trick;
use App\Form\TrickType;
use App\Repository\CommentRepository;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/trick")
 */
class TrickController extends AbstractController
{
    /**
     * @Route("/new", name="trick_new", methods={"GET","POST"})
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $trick = new Trick();
        $form = $this->createForm(TrickType::class, $trick);

        $form->handleRequest($request);

        $entityManager = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $imageFile = $form->get('image')->getData();
                $imageCollection = $form->get('images')->getData();

                if ($imageFile) {
                    $imageName = $fileUploader->upload($imageFile);
                    $trick->setImageName($imageName);
                }

                if ($imageCollection) {

                    foreach ($imageCollection as $image) {
                        $newFilename = $fileUploader->upload($image->getImage());
                        $image->setImageName($newFilename);
                        $image->setTrick($trick);
                        $image->setUpdatedAt(new \DateTimeImmutable('now'));
                        $entityManager->persist($image);
                        $trick->addImage($image);
                    }

                }

                $entityManager->persist($trick);
                $entityManager->flush();
                $this->addFlash('success', 'This post is now public');
            } catch (Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
                return $this->redirectToRoute('trick_show', [
                    'slug' => $trick->getSlug(),
                    'id' => $trick->getId(),
                    'page' => '1'
                ]);

            }

            return $this->redirectToRoute('trick_show', [
                'slug' => $trick->getSlug(),
                'id' => $trick->getId(),
                'page' => '1'
            ]);
        }

        return $this->render('trick/new.html.twig', [
            'trick' => $trick,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{slug}-{id}/{page}", name="trick_show", methods={"GET"},  requirements={
     *         "slug": "[a-z0-9\-]*",
     *          "page" = "\d+"
     *     })
     */
    public function show(Trick $trick, CommentRepository $commentRepository, $page): Response
    {
        $comments = $commentRepository->findAllPaginateByTrick($page, $trick);

        return $this->render('trick/show.html.twig', [
            'page' => $page,
            'trick' => $trick,
            'comments' => $comments,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="trick_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Trick $trick, FileUploader $fileUploader): Response
    {
        $form = $this->createForm(TrickType::class, $trick);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            try{
                $imageFile = $form->get('image')->getData();
                $imageCollection = $form->get('images')->getData();

                $entityManager = $this->getDoctrine()->getManager();

                if ($imageFile) {
                    $imageName = $fileUploader->upload($imageFile);
                    $trick->setImageName($imageName);
                }

                if ($imageCollection) {

                    foreach($imageCollection as $image) {
                        if ($image->getImage()) {
                            $newFilename = $fileUploader->upload($image->getImage());
                            $image->setImageName($newFilename);
                            $image->setTrick($trick);
                            $image->setUpdatedAt(new \DateTimeImmutable('now'));
                            $entityManager->persist($image);
                            $trick->addImage($image);
                        }
                    }
                }

                $images = $entityManager->getRepository(Image::class)->findBy(['trick'=> $trick]);
                if ($images) {
                    foreach ($images as $image) {
                        if (!in_array($image, $imageCollection->getValues())) {
                            $filesystem = new Filesystem();
                            $imagePath = $this->getParameter('images_directory') . '/' . $image->getImageName();
                            $filesystem->remove($imagePath);
                        }
                    }
                }

                $entityManager->persist($trick);
                $entityManager->flush();

                $this->addFlash('success', 'This post is updated');
            }  catch (Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
                return $this->redirectToRoute('trick_show', [
                    'slug' => $trick->getSlug(),
                    'id' => $trick->getId(),
                    'page' => '1'
                ]);
            }
            return $this->redirectToRoute('trick_show', [
                'slug' => $trick->getSlug(),
                'id' => $trick->getId(),
                'page' => '1'
            ]);
        }

        return $this->render('trick/edit.html.twig', [
            'trick' => $trick,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="trick_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Trick $trick): Response
    {
        if ($this->isCsrfTokenValid('delete'.$trick->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($trick);
            $entityManager->flush();
            $this->addFlash('success', 'This post has been deleted');
        }

        return $this->redirectToRoute('home', ['page' => '1']);
    }
}
