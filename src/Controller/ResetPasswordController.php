<?php

namespace App\Controller;


use App\Form\ChangePasswordFormType;
use App\Form\ResetPasswordFormType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class ResetPasswordController extends AbstractController
{

    private $userRepository;

    public function __construct
    (
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/reset-password", name="app_password_reset")
     * @param Request $request
     * @throws \Exception
     */
    public function requestResetingPassword(Request $request, \Swift_Mailer $mailer) : Response
    {
        $form = $this->createForm(ResetPasswordFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
                $email = $form->get('email')->getData();
                // is email is in database ?
                $user = $this->userRepository->findOneBy(['email' => $email]);
                if (!$user) {
                    $this->addFlash('error', 'This email is not in database');
                    return $this->render('reset_password/request.html.twig',[
                        'requestForm' => $form->createView()
                    ]);
                }

            // if email is corresponding to a user, generate a token, update user and send email
            $token = bin2hex(random_bytes(24));
            $now = new \DateTimeImmutable('now');
            $now = $now->add(new \DateInterval('PT10M'));

            $user->setToken($token);
            $user->setExpiredDateTimeToken($now);
            $this->getDoctrine()->getManager()->flush();

            try {
                $message = (new \Swift_Message('Your password reset request'))
                    ->setFrom('mathieuhdb@gmail.com', 'SnowTricks')
                    ->setTo($form->get('email')->getData())
                    ->setBody(
                        $this->renderView(
                            'reset_password/email.html.twig', [
                                'resetToken' => $token,
                                'email' => $email
                            ]
                        ),
                        'text/html'
                    );

                $mailer->send($message);

            } catch (Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
                return $this->render('reset_password/request.html.twig',[
                    'requestForm' => $form->createView()
                ]);
            }

            return $this->render('reset_password/check_email.html.twig', [
                'resetToken' => $token
            ]);

        }

        return $this->render('reset_password/request.html.twig',[
            'requestForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/reset-password/reset", name="app_reset_password")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function resetPassword(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $form = $this->createForm(ChangePasswordFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // is user with this $token is in database ?
            $token = $request->query->get('token');
            $email = $request->query->get('email');

            $user = $this->userRepository->findOneBy(['token' => $token, 'email' => $email]);
            if (!$user) {
                $this->addFlash('error', 'This email is not in database');
                return $this->render('reset_password/request.html.twig',[
                    'requestForm' => $form->createView()
                ]);
            }
            $now = new \DateTimeImmutable('now');
            if ($user->getExpiredDateTimeToken() < $now) {
                $this->addFlash('error', 'Your token is expired, try again');
                return $this->redirectToRoute('app_password_reset');
            }

            try {
                // encode the plain password
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );
                $user->setUpdatedAt(new \DateTimeImmutable('now'));
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash('success', 'Your password is changed');
                return $this->render('reset_password/success.html.twig');

            } catch (Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
                return $this->render('reset_password/request.html.twig', [
                    'requestForm' => $form->createView()
                ]);
            }
        }
        return $this->render('reset_password/reset.html.twig',[
            'resetForm' => $form->createView()
        ]);
    }

}
