<?php
declare(strict_types = 1);

namespace App\Controller;

use App\Repository\TrickRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private $trickRepository;

    public function __construct
    (
        TrickRepository $trickRepository
    )
    {
        $this->trickRepository = $trickRepository;
    }

    /**
     * @Route("/{page}", name="home", methods={"GET"},  requirements={"page" = "\d+"}))
     */
    public function home(int $page = 1)
    {
        $tricks = $this->trickRepository->findAllPaginate($page);

        return $this->render('home.html.twig',[
            'page' => $page,
            'tricks' => $tricks
        ]);
    }

}