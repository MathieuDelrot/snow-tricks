<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\TrickRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/comment")
 */
class CommentController extends AbstractController
{

    /**
     * @Route("/new/{trick_id}/{user_id}", name="comment_new", methods={"GET","POST"})
     */
    public function new(Request $request, TrickRepository $trickRepository, UserRepository $userRepository, EntityManagerInterface $entityManager): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        $trickId = $request->get('trick_id');
        $trick = $trickRepository->find($trickId);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setTrick($trick);
            $comment->setUser($userRepository->find($this->getUser()));
            $entityManager->persist($comment);
            $entityManager->flush();

            $this->addFlash('success', 'This comment is added');
            return $this->redirectToRoute('trick_show', [
                'slug' => $trick->getSlug(),
                'id' => $trick->getId(),
                'page' => '1'
            ]);
        }

        return $this->render('comment/new.html.twig', [
            'comment' => $comment,
            'slug' => $trick->getSlug(),
            'trick_id' => $trickId ,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="comment_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Comment $comment): Response
    {
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setUpdatedAt(new \DateTimeImmutable('now'));
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'This comment is updated');
            return $this->redirectToRoute('trick_show', [
                'slug' => $comment->getTrick()->getSlug(),
                'id' => $comment->getTrick()->getId(),
                'page' => '1',
            ]);

        }

       return $this->render('comment/edit.html.twig', [
           'slug' => $comment->getTrick()->getSlug(),
           'trick_id' => $comment->getTrick()->getId(),
           'comment' => $comment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="comment_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Comment $comment): Response
    {
        if ($this->isCsrfTokenValid('delete'.$comment->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comment);
            $entityManager->flush();
        }

        $this->addFlash('success', 'This comment is updated');
        return $this->redirectToRoute('trick_show', [
            'slug' => $comment->getTrick()->getSlug(),
            'id' => $comment->getTrick()->getId(),
            'page' => '1'
        ]);
    }
}
