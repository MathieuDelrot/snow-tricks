<?php

namespace App\DataFixtures;

use App\Entity\Trick;
use App\Entity\TricksType;
use App\Entity\User;
use App\Entity\Video;
use App\Repository\TrickRepository;
use App\Repository\TricksTypeRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AppFixtures extends Fixture
{

    private $passwordEncoder;
    private $trickRepository;
    private $userRepository;
    private $tricksTypeRepository;


    public function __construct
    (
        UserPasswordEncoderInterface $passwordEncoder,
        TrickRepository $trickRepository,
        UserRepository $userRepository,
        TricksTypeRepository $tricksTypeRepository
    )
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->trickRepository = $trickRepository;
        $this->userRepository = $userRepository;
        $this->tricksTypeRepository = $tricksTypeRepository;

    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setPassword($this->passwordEncoder->encodePassword($user,'030314'));
        $user->setEmail('mathieuhdb@gmail.com');
        $user->setUserName('Mat');
        $user->setRoles(['ROLE_USER']);
        $user->setCreatedAt(new \DateTimeImmutable('now'));
        $manager->persist($user);
        $manager->flush();

        $tricksType = new TricksType();
        $tricksType->setTitle('Slides');
        $manager->persist($tricksType);
        $slides = $tricksType;

        $tricksType = new TricksType();
        $tricksType->setTitle('Grabs');
        $manager->persist($tricksType);
        $grabs = $tricksType;

        $tricksType = new TricksType();
        $tricksType->setTitle('Spins');
        $manager->persist($tricksType);
        $spins = $tricksType;

        $trick = new Trick();
        $trick->setTitle('Method');
        $trick->setSubTitle("A fundamental trick performed by bending the knees to lift the board behind the rider's back, and grabbing the heel edge of the snowboard with the leading hand");
        $trick->setDescription("<p><strong>Variations on the method include :</strong></p>
            <p>Power method, cross bone, or Palmer method</p>
            <p>Performed by grabbing the heel edge with the leading hand, and tucking up the board while kicking out the rear foot in such a way that the base of the board is facing forward. 
            Derived from the snowboarder Chris Roach of Grass Valley, CA. Other notable riders who popularized this air include snowboarders Jamie Lynn, Shaun Palmer, Terry Kidwell, and skateboarders Steve Caballero and Christian Hosoi.</p>");
        $trick->setTricksType($grabs);
        $trick->setCreatedAt(new \DateTimeImmutable('now'));
        $manager->persist($trick);

        $video = new Video();
        $video->setName('How to Method Grab');
        $video->setEmbededCode("<iframe width='560' height='315' src='https://www.youtube.com/embed/_Cfssjuv0Zg' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>");
        $video->setTrick($trick);

        $arrayCollection = new ArrayCollection([$video]);
        $trick->setVideos($arrayCollection);

        $trick = new Trick();
        $trick->setTitle('Mule kick');
        $trick->setSubTitle('An early snowboarder adaptation of the skateboarders method air.');
        $trick->setDescription("<p>Often called a Toyota air, after its similar posturing to the early 1980s Toyota 'Oh What A Feeling' ad campaign featuring people jumping off the ground, performed by jumping into an aerial backbend with legs bending until nearly kicking yourself in the butt as with skiing's backscratcher air, both arms bent back high over the head and not grabbing the board. Still occasionally seen and widely regarded as terrible.</p>");
        $trick->setTricksType($grabs);
        $trick->setCreatedAt(new \DateTimeImmutable('now'));
        $manager->persist($trick);


        $trick = new Trick();
        $trick->setTitle('Nose press');
        $trick->setSubTitle('A jib in which the snowboard remains parallel with the box or rail, and the rider shifts their weight to the nose of the board, lifting the tail off the surface.');
        $trick->setDescription('<p>A jib in which the snowboard remains parallel with the box or rail, and the rider shifts their weight to the nose of the board, lifting the tail off the surface.</p>');
        $trick->setTricksType($grabs);
        $trick->setCreatedAt(new \DateTimeImmutable('now'));
        $manager->persist($trick);

        $video = new Video();
        $video->setName('How to Method Grab');
        $video->setEmbededCode("<iframe width='560' height='315' src='https://www.youtube.com/embed/Px2YuKQVS_g' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>");
        $video->setTrick($trick);

        $arrayCollection = new ArrayCollection([$video]);
        $trick->setVideos($arrayCollection);

        $trick = new Trick();
        $trick->setTitle('Taipan air');
        $trick->setSubTitle('The back hand grabs the toe edge just in front of the rear foot');
        $trick->setDescription('<p>However, the arm must go around the outside of your rear knee. The board is then pulled behind the rider (tweaked). The name Taipan is a portmanteau of tail/Japan air.</p>');
        $trick->setTricksType($grabs);
        $trick->setCreatedAt(new \DateTimeImmutable('now'));
        $manager->persist($trick);

        $trick = new Trick();
        $trick->setTitle('Backside Misty');
        $trick->setSubTitle('After a rider learns the basic backside 540 off the toes, the Misty Flip can be an easy next progression step.');
        $trick->setDescription('<p>Misty Flip is quite different than the backside rodeo, because instead of corking over the heel edge with a back flip motion, the Misty corks off the toe edge specifically and has more of a Front Flip in the beginning of the trick, followed by a side flip coming out to the landing.</p>');
        $trick->setTricksType($spins);
        $trick->setCreatedAt(new \DateTimeImmutable('now'));
        $manager->persist($trick);

        $trick = new Trick();
        $trick->setTitle('Frontside Misty');
        $trick->setSubTitle('The Frontside misty ends up looking quite a bit like a frontside rodeo in the middle of the trick, but at take off the rider uses a more frontflip type of motion to start the trick.');
        $trick->setDescription('The frontside Misty can only be done off the toes and the rider will wind up to spin frontside, then snap their trailing shoulder towards their front foot and the lead shoulder will release towards the sky. as they unwind at takeoff release. Usually grabbing Indy the rider follows the lead shoulder through the rotation to 540, 720 and even 900.');
        $trick->setTricksType($spins);
        $trick->setCreatedAt(new \DateTimeImmutable('now'));
        $manager->persist($trick);

        $trick = new Trick();
        $trick->setTitle('Frontside Rodeo');
        $trick->setSubTitle('The basic frontside rodeo is all together a 540');
        $trick->setDescription('It essentially falls into a grey area between an off axis frontside 540 and a frontside 180 with a back flip blended into it. The grab choice and different line and pop factors can make it more flipy or more of an off-axis spin. Frontside rodeo can be done off the heels or toes and with a little more spin on the Z Axis can go to 720 or 900. It is possible to do it to a 1080 but, if there is too much flip in the spin, it can be hard not to over flip when going past 720 and 900. The bigger the Z Axis spin, the later the inverted part of the rotation should be. Gaining control on big spin rodeos, may lead to a double cork or a second flip rotation in the spin, if the rider has developed a comfort level with double flips on the tramp or other gymnastic environment.;Rodeo flip; frontside rodeo: A frontward-flipping frontside spin done off the toe-edge. 
        Most commonly performed with a 540° rotation, but also performed as a 720°, 900°, etc..');
        $trick->setTricksType($spins);
        $trick->setCreatedAt(new \DateTimeImmutable('now'));
        $manager->persist($trick);

        $video = new Video();
        $video->setName('How to Method Grab');
        $video->setEmbededCode("<iframe width='560' height='315' src='https://www.youtube.com/embed/HEzgU_A16Gs' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>");
        $video->setTrick($trick);

        $arrayCollection = new ArrayCollection([$video]);
        $trick->setVideos($arrayCollection);


        $trick = new Trick();
        $trick->setTitle('Boardslide');
        $trick->setSubTitle('A slide performed where the riders leading foot passes over the rail on approach, with their snowboard traveling perpendicular along the rail or other obstacle.');
        $trick->setDescription('When performing a frontside boardslide, the snowboarder is facing uphill. When performing a backside boardslide, a snowboarder is facing downhill. This is often confusing to new riders learning the trick because with a frontside boardslide you are moving backward and with a backside boardslide you are moving forward.');
        $trick->setTricksType($slides);
        $trick->setCreatedAt(new \DateTimeImmutable('now'));
        $manager->persist($trick);

        $trick = new Trick();
        $trick->setTitle('Noseblunt');
        $trick->setSubTitle("A slide performed where the rider's trailing foot passes over the rail on approach, with their snowboard traveling perpendicular and leading foot directly above the rail or other obstacle (like a noseslide)");
        $trick->setDescription('When performing a frontside noseblunt, the snowboarder is facing downhill. When performing a backside noseblunt, the snowboarder is facing uphill.');
        $trick->setTricksType($slides);
        $trick->setCreatedAt(new \DateTimeImmutable('now'));
        $manager->persist($trick);

        $trick = new Trick();
        $trick->setTitle('The Gutterball');
        $trick->setSubTitle("The Gutterball is a one footed (front foot is strapped in and the rear foot is unstrapped ) front boardslide with a backhanded seatbelt nose grab, resembling the body position that someone would have after releasing a bowling ball down a bowling ally.");
        $trick->setDescription('This trick was invented and named by Jeremy Cameron which won him a first place in the Morrow Snowboards "FAME WAR" Best Trick contest in 2009.');
        $trick->setTricksType($slides);
        $trick->setCreatedAt(new \DateTimeImmutable('now'));
        $manager->persist($trick);


        $manager->flush();
    }
}
