<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TricksTypeRepository")
 */
class TricksType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\trick", mappedBy="tricks_type")
     */
    private $trick;

    public function __construct()
    {
        $this->trick = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|trick[]
     */
    public function getTrick(): Collection
    {
        return $this->trick;
    }

    public function addTrick(trick $trick): self
    {
        if (!$this->trick->contains($trick)) {
            $this->trick[] = $trick;
            $trick->setTricksType($this);
        }

        return $this;
    }

    public function removeTrickId(trick $trick): self
    {
        if ($this->trick->contains($trick)) {
            $this->trick->removeElement($trick);
            // set the owning side to null (unless already changed)
            if ($trick->getTricksType() === $this) {
                $trick->setTricksType(null);
            }
        }

        return $this;
    }

}
