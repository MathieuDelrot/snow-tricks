<?php
declare(strict_types = 1);


namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ConnectAndAddATrickTest extends WebTestCase
{

    public function testConnectAndAddATrick()
    {
        $client = static::createClient();

        // Connect a user
        $crawler = $client->request('GET', '/login');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Sign in')->form();

        $form['email']  = 'mathieuhdb@gmail.com';
        $form['password'] = 'pswd';

        $client->submit($form);


        //Go to add Trick page and Add one
        $crawler = $client->request('GET', '/trick/new');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Save')->form();

        $form['trick[title]']  = 'Adding trick test';
        $form['trick[subTitle]'] = 'this is the subtitle';
        $form['trick[description]'] = 'this is the description';

        $client->submit($form);

    }

}