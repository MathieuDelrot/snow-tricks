<?php
declare(strict_types = 1);

namespace App\Tests\Controller;

use App\Entity\Trick;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTestContentPage extends WebTestCase
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testShowTricksOnHomePage()
    {

        /** @var Trick $trick */
        $trick = $this->entityManager
            ->getRepository(Trick::class)
            ->findOneBy(['id' => 'DESC']);
        $title = $trick->getTitle();

        $this->assertSelectorTextContains('html h3', $title);

    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }

}