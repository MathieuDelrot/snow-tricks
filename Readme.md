# Snowtricks

Snwotrick is a Symfony website about snowboard Tricks. All Tricks are added by member of the website.

## Installation

1 :Launch the following command to download the project from gitlab.

```bash
git@gitlab.com:MathieuDelrot/snow-tricks.git
```

2 :In your terminal go to the project folder and then go to docker folder, now you can build docker containers

```bash
docker-compose up --build
```

3 :Create database with following command in the docker folder

```bash
docker-compose exec php-fpm doctrine:database:create
```

4 :Create schema with following command in the docker folder

```bash
docker-compose exec php-fpm doctrine:schema:create
```

5 :Launch 10 first tricks with following command in the docker folder

```bash
docker-compose exec php-fpm doctrine:fixtures:load
```

6 :Access to website by typing http://localhost/ into your browser



## Quality code badge from Codacy
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/a57416d4e4da4668863849ea9f40aa41)](https://www.codacy.com/gl/MathieuDelrot/snow-tricks/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=MathieuDelrot/snow-tricks&amp;utm_campaign=Badge_Grade)